This project aims to deliver recommendations for similar artists you might like and list their songs with their respective lyrics.

Input a band or artist in the searchbar and after clicking on "Let's go!" the app will show you some recommendations in the table below. 
Click on a song and if available the lyrics of that particular song will appear below.

Get started:
Fork, Clone or Download this Repo on Gitlab.

Clone the Repo: `git@git.thm.de:tldb06/media4you.git`