/*!
    * Start Bootstrap - Freelancer v6.0.4 (https://startbootstrap.com/themes/freelancer)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-freelancer/blob/master/LICENSE)
    */
/* CookieConsent */
const cookieContainer = document.querySelector(".cookie-container");
const cookieButton = document.querySelector(".cookie-btn");

cookieButton.addEventListener("click", () => {
    cookieContainer.classList.remove("active");
    localStorage.setItem("cookieBannerDisplayed", "true");
});

setTimeout(() => {
    if (!localStorage.getItem("cookieBannerDisplayed"))
        cookieContainer.classList.add("active");
}, 2000);

/* Ende */

/* API JS */

const tasteKey = "383307-Media4Yo-2S9Z9GXZ";
const musixmatchKey = "0d5fc7743a71e8ba5a6fb0498b58c212";
const apiseedsKey = "FDbw71lFHiWMxBLYjwxCPKBk0oAwXSCNFJYyGzDh30CwujJLhgbEM9OKvtvtm8l4";

const idArtist1 = document.querySelector("#artist1");
const idArtist2 = document.querySelector("#artist2");
const idArtist3 = document.querySelector("#artist3");
const idArtist4 = document.querySelector("#artist4");
const idArtist5 = document.querySelector("#artist5");


const idSong11 = document.querySelector("#song11");
const idSong12 = document.querySelector("#song12");
const idSong13 = document.querySelector("#song13");
const idSong14 = document.querySelector("#song14");
const idSong15 = document.querySelector("#song15");

const idSong21 = document.querySelector("#song21");
const idSong22 = document.querySelector("#song22");
const idSong23 = document.querySelector("#song23");
const idSong24 = document.querySelector("#song24");
const idSong25 = document.querySelector("#song25");

const idSong31 = document.querySelector("#song31");
const idSong32 = document.querySelector("#song32");
const idSong33 = document.querySelector("#song33");
const idSong34 = document.querySelector("#song34");
const idSong35 = document.querySelector("#song35");

const idSong41 = document.querySelector("#song41");
const idSong42 = document.querySelector("#song42");
const idSong43 = document.querySelector("#song43");
const idSong44 = document.querySelector("#song44");
const idSong45 = document.querySelector("#song45");

const idSong51 = document.querySelector("#song51");
const idSong52 = document.querySelector("#song52");
const idSong53 = document.querySelector("#song53");
const idSong54 = document.querySelector("#song54");
const idSong55 = document.querySelector("#song55");


const idLyricsP = document.querySelector("#lyricsP");
const idLyrics = document.querySelector("#lyrics");


const music = {
    artistSearched : "",

    artist1 : "",
    artist2 : "",
    artist3 : "",
    artist4 : "",
    artist5 : "",

    song11 : "",
    song12 : "",
    song13 : "",
    song14 : "",
    song15 : "",

    song21 : "",
    song22 : "",
    song23 : "",
    song24 : "",
    song25 : "",

    song31 : "",
    song32 : "",
    song33 : "",
    song34 : "",
    song35 : "",

    song41 : "",
    song42 : "",
    song43 : "",
    song44 : "",
    song45 : "",

    song51 : "",
    song52 : "",
    song53 : "",
    song54 : "",
    song55 : "",

    lyricsS11 : "",
    lyricsS12 : "",
    lyricsS13 : "",
    lyricsS14 : "",
    lyricsS15 : "",

    lyricsS21 : "",
    lyricsS22 : "",
    lyricsS23 : "",
    lyricsS24 : "",
    lyricsS25 : "",

    lyricsS31 : "",
    lyricsS32 : "",
    lyricsS33 : "",
    lyricsS34 : "",
    lyricsS35 : "",

    lyricsS41 : "",
    lyricsS42 : "",
    lyricsS43 : "",
    lyricsS44 : "",
    lyricsS45 : "",

    lyricsS51 : "",
    lyricsS52 : "",
    lyricsS53 : "",
    lyricsS54 : "",
    lyricsS55 : ""
};

/* Button press , get artistSearched*/
function button() {
    music.artistSearched = document.getElementById("userInput").value;
    getMusic(music.artistSearched);
}

/* Ende */

function displayArtist() {
    idArtist1.innerHTML = music.artist1;
    idArtist2.innerHTML = music.artist2;
    idArtist3.innerHTML = music.artist3;
    idArtist4.innerHTML = music.artist4;
    idArtist5.innerHTML = music.artist5;
}

function displaySongs() {
    idSong11.innerHTML = music.song11;
    idSong12.innerHTML = music.song12;
    idSong13.innerHTML = music.song13;
    idSong14.innerHTML = music.song14;
    idSong15.innerHTML = music.song15;

    idSong21.innerHTML = music.song21;
    idSong22.innerHTML = music.song22;
    idSong23.innerHTML = music.song23;
    idSong24.innerHTML = music.song24;
    idSong25.innerHTML = music.song25;

    idSong31.innerHTML = music.song31;
    idSong32.innerHTML = music.song32;
    idSong33.innerHTML = music.song33;
    idSong34.innerHTML = music.song34;
    idSong35.innerHTML = music.song35;

    idSong41.innerHTML = music.song41;
    idSong42.innerHTML = music.song42;
    idSong43.innerHTML = music.song43;
    idSong44.innerHTML = music.song44;
    idSong45.innerHTML = music.song45;

    idSong51.innerHTML = music.song51;
    idSong52.innerHTML = music.song52;
    idSong53.innerHTML = music.song53;
    idSong54.innerHTML = music.song54;
    idSong55.innerHTML = music.song55;
}

function displayLyrics() {
    idLyricsP.innerHTML = music.lyricsS11;
    idLyrics.innerHTML = "Your Lyrics:"
}

function getSongs(artist1, artist2, artist3, artist4, artist5) {
    let apiMusixmatch1 = `http://api.musixmatch.com/ws/1.1/track.search?q_artist=${artist1}&page_size=5&page=1&s_track_rating=desc&apikey=${musixmatchKey}`;
    fetch(apiMusixmatch1).then(function (response) {
        let dataMusixmatch1 = response.json();
        return dataMusixmatch1;
}).then( function (dataMusixmatch1) {
        music.song11 = dataMusixmatch1.message.body.track_list[0].track.track_name;
        music.song12 = dataMusixmatch1.message.body.track_list[1].track.track_name;
        music.song13 = dataMusixmatch1.message.body.track_list[2].track.track_name;
        music.song14 = dataMusixmatch1.message.body.track_list[3].track.track_name;
        music.song15 = dataMusixmatch1.message.body.track_list[4].track.track_name;
    }).then(function () {
        let apiMusixmatch2 = `http://api.musixmatch.com/ws/1.1/track.search?q_artist=${artist2}&page_size=5&page=1&s_track_rating=desc&apikey=${musixmatchKey}`;
        fetch(apiMusixmatch2).then(function (response) {
            let dataMusixmatch2 = response.json();
            return dataMusixmatch2;
        }).then( function (dataMusixmatch2) {
            music.song21 = dataMusixmatch2.message.body.track_list[0].track.track_name;
            music.song22 = dataMusixmatch2.message.body.track_list[1].track.track_name;
            music.song23 = dataMusixmatch2.message.body.track_list[2].track.track_name;
            music.song24 = dataMusixmatch2.message.body.track_list[3].track.track_name;
            music.song25 = dataMusixmatch2.message.body.track_list[4].track.track_name;
        })}).then(function () {
            let apiMusixmatch3 = `http://api.musixmatch.com/ws/1.1/track.search?q_artist=${artist3}&page_size=5&page=1&s_track_rating=desc&apikey=${musixmatchKey}`;
            fetch(apiMusixmatch3).then(function (response) {
                let dataMusixmatch3 = response.json();
                return dataMusixmatch3;
            }).then( function (dataMusixmatch3) {
                music.song31 = dataMusixmatch3.message.body.track_list[0].track.track_name;
                music.song32 = dataMusixmatch3.message.body.track_list[1].track.track_name;
                music.song33 = dataMusixmatch3.message.body.track_list[2].track.track_name;
                music.song34 = dataMusixmatch3.message.body.track_list[3].track.track_name;
                music.song35 = dataMusixmatch3.message.body.track_list[4].track.track_name;
            })}).then(function () {
                let apiMusixmatch4 = `http://api.musixmatch.com/ws/1.1/track.search?q_artist=${artist4}&page_size=5&page=1&s_track_rating=desc&apikey=${musixmatchKey}`;
                fetch(apiMusixmatch4).then(function (response) {
                    let dataMusixmatch4 = response.json();
                    return dataMusixmatch4;
                }).then( function (dataMusixmatch4) {
                    music.song41 = dataMusixmatch4.message.body.track_list[0].track.track_name;
                    music.song42 = dataMusixmatch4.message.body.track_list[1].track.track_name;
                    music.song43 = dataMusixmatch4.message.body.track_list[2].track.track_name;
                    music.song44 = dataMusixmatch4.message.body.track_list[3].track.track_name;
                    music.song45 = dataMusixmatch4.message.body.track_list[4].track.track_name;
                })}).then(function () {
                    let apiMusixmatch5 = `http://api.musixmatch.com/ws/1.1/track.search?q_artist=${artist5}&page_size=5&page=1&s_track_rating=desc&apikey=${musixmatchKey}`;
                    fetch(apiMusixmatch5).then(function (response) {
                        let dataMusixmatch5 = response.json();
                        return dataMusixmatch5;
                    }).then( function (dataMusixmatch5) {
                        music.song51 = dataMusixmatch5.message.body.track_list[0].track.track_name;
                        music.song52 = dataMusixmatch5.message.body.track_list[1].track.track_name;
                        music.song53 = dataMusixmatch5.message.body.track_list[2].track.track_name;
                        music.song54 = dataMusixmatch5.message.body.track_list[3].track.track_name;
                        music.song55 = dataMusixmatch5.message.body.track_list[4].track.track_name;
                    })});
}



function getMusic(artistSearched) {
    let apiTaste = `https://tastedive.com/api/similar?q=${artistSearched}&k=${tasteKey}`;
    fetch(apiTaste).then(function (response) {
        let dataTaste = response.json();
        return dataTaste;
    }).then( function (dataTaste) {
        music.artist1 = dataTaste.Similar.Results[0].Name;
        music.artist2 = dataTaste.Similar.Results[1].Name;
        music.artist3 = dataTaste.Similar.Results[2].Name;
        music.artist4 = dataTaste.Similar.Results[3].Name;
        music.artist5 = dataTaste.Similar.Results[4].Name;
    }).then(function () {
        displayArtist();
    }).then( function () {
        getSongs(music.artist1, music.artist2, music.artist3, music.artist4, music.artist5);
    }).then(function () {
        displaySongs();
    });
}

function lyrics(x, y) {
    let apiApiSeeds = `https://orion.apiseeds.com/api/music/lyric/${y.innerHTML}/${x.innerHTML}?apikey=${apiseedsKey}`;
    fetch(apiApiSeeds).then(function (response) {
        let dataApiSeeds = response.json();
        return dataApiSeeds;
    }).then( function (dataApiSeeds) {
        music.lyricsS11 = dataApiSeeds.result.track.text;
    }).then(function () {
        displayLyrics();
    });
}

/* Ende */

(function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 71)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Scroll to top button appear
    $(document).scroll(function () {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 80
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

    // Floating label headings for the contact form
    $(function () {
        $("body").on("input propertychange", ".floating-label-form-group", function (e) {
            $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
        }).on("focus", ".floating-label-form-group", function () {
            $(this).addClass("floating-label-form-group-with-focus");
        }).on("blur", ".floating-label-form-group", function () {
            $(this).removeClass("floating-label-form-group-with-focus");
        });
    });

})(jQuery); // End of use strict


